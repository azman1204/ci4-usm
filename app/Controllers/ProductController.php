<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ProductModel;

class ProductController extends BaseController
{
    // list semua product
    public function index() {
        $model = new ProductModel(); // create object
        // findAll() - return banyak rekod(array)
        $rows = $model->findAll(); // select * from product
        //var_dump($rows);
        return view('product/list', ['rows' => $rows]);
    }

    // show form
    public function create() {
        $prod = [
            'id'   => '',
            'name' => '',
            'description' => '',
            'price' => 0
        ];
        return view('product/form', ['prod' => $prod]);
    }

    // save data
    function save() {
        $req = $this->request;

        $message = [
            'name' => [
                'required'   => 'Nama wajib diisi',
                'min_length' => 'Nama mesti sekurang-kurangnya 3 karakter'
            ],
            'description' => ['required' =>'Keterangan wajib diisi']
        ];

        // validate data
        $ok = $this->validate([
            'name' => 'required|min_length[3]',
            'description' => 'required',
            'price' => 'required'
        ], $message);

        if (! $ok) {
            // validation x lulus
            //echo 'Gagal validation';exit;
            $prod = $req->getPost();
            return view('product/form', [
                'validator' => $this->validator, 
                'prod' => $prod
            ]);
        }

        // read semua data dari form
        // getPost() - return semua data dari form dlm bentuk arr
        //var_dump($req->getPost());
        $data = $req->getPost();
        $model = new ProductModel();
        $model->save($data); // insert into() values()
        return redirect()->to('/product-list');
    }

    function edit($id) {
        $model = new ProductModel();
        // find() - return satu rekod (array)
        $prod = $model->find($id);
        //var_dump($prod);
        return view('product/form', 
        ['prod' => $prod]);
    }

    function delete($id) {
        (new ProductModel())->delete($id);
        return redirect()->to('/product-list');
    }
}
