<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\Calculator;
use App\Models\ProductModel;
use App\Models\UserModel;
use App\ThirdParty\Printing;
use Dompdf\Dompdf;

class HelperDemoController extends BaseController
{
    function ajaxCall($email) {
        $user = model('UserModel');
        $row = $user->where('email', $email)->first();
        if ($row) {
            return 'user dah wujud';
        } else {
            return 'User belum wujud';
        }
        //return date('d-m-Y H:i:s');
    }

    function queryBuilder() {
        $db = \Config\Database::connect();
        $rows = $db->table('products')
                ->select('*')
                ->join('users', 'products.created_by = users.id')
                ->get()
                ->getResult();
        var_dump($rows);
    }

    function joinModel() {
        $model = new ProductModel();
        $products = $model->findAll();
        foreach ($products as $pro) {
            $muser = new UserModel();
            $user = $muser->find($pro['created_by']);
            echo "{$user['name']} {$pro['name']}";
        }
    }

    function genPdf() {
        $dompdf = new Dompdf();
        //$dompdf->loadHtml("<h1>Hello World</h1>");
        $html = view('demo_pdf', ['nama' => 'John Doe']);
        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->stream();
    }

    public function index()
    {
        //helper('util');
        helper(['util', 'form']);
        echo sum(5, 4);
        echo minus(10,3);
    }

    function calculateNewSalary() {
        $calc = new Calculator();
        $new_salary = $calc->increaseSalary(10000, 13);
        echo $new_salary;
    }

    function print() {
        Printing::instant();
    }
}
