<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class LoginController extends BaseController
{
    // show login form
    function login() {
        helper('form');
        return view('login/form');
    }

    // check username / pwd dgn DB
    function auth() {
        $req = request();
        $emel = $req->getPost('email');
        $password = $req->getPost('password');
        $model = model('UserModel');
        // return an array
        $user = $model->where('email', $emel)->first();
        //var_dump($user);
        if (! $user) {
            // null / emel x wujud
            // with() - session flash data 
            return redirect()
                    ->to('/login')
                    ->withInput()
                    ->with('msg', 'Emel tidak wujud');
        } else {
            // emel wujud.. then check password
            $ok = password_verify($password, $user['password']);
            if ($ok) {
                // pwd match
                session()->set('isloggedin', true);
                session()->set('role', $user['role']);
                return redirect()->to('/user-list');
            } else {
                // pwd x match
                return redirect()
                    ->to('/login')
                    ->withInput()
                    ->with('msg', 'Password tidak match');
            }
        }
    }

    function logout() {
        session()->destroy();
        return redirect()->to('/login');
    }
}
