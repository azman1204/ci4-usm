<?php
namespace App\Controllers;
use App\Models\NewsModel;
use App\Filters\AuditFilter;

class NewsController extends BaseController {
    public function index() {
        $model = new NewsModel();
        echo 'coming soon...';
        $audit = new AuditFilter();
        $audit->before($this->request);
    }

    public function save() {
        echo 'saving data..';
    }

    public function create() {
        return view('news/form'); // Views/news/form.php
    }
}