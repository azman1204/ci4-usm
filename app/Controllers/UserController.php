<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class UserController extends BaseController
{
    var $model;

    // constructor
    function __construct() {
        helper(['form', 'util']); // load helper
        $this->model = model('UserModel');
    }

    // list all users
    function index() {
        //$model = model('UserModel');
        // $users = $this->model->findAll();
        $per_page = 3;
        $users = $this->model
                 ->orderBy('name')
                 ->paginate($per_page);
        $pager = $this->model->pager;
        $data = $this->request->getGet();
        // .../user-list?page=2
        $page = isset($data['page']) ? $data['page'] : 1;
        $no = ($page - 1) * $per_page + 1;
        return view('user/list', [
            'users' => $users,
            'pager' => $pager,
            'no'    => $no
        ]);
    }

    function save() {
        $data = request()->getPost();
        $rules = $this->model->validationRules;
        $rules['email'] = $rules['email'] . "|is_unique[users.email, id, {$data['id']}]";
        // if ($data['id'] !== '') {
        //     // cubaan update
        //     $ori = $this->model->find($data['id']);
        //     if ($data['email'] !== $ori['email']) {
        //         $v = 'is_unique[users.email]';
        //         $rules['email'] = $rules['email'] . "|$v";
        //     }
        // } else {
        //     // cubaan insert
        //     $rules['email'] = $rules['email'] . "|is_unique[users.email]";
        // }

        // validation berlaku disini
        $ok = $this->validate(
            $rules,
            $this->model->validationMessages
        );
        
        if (! $ok) {
            // if (! $this->model->save($data)) {
            // gagal validation
            return view('user/form', [
                'data' => $data,
                'validator' => $this->validator
            ]);
        } 

        $this->model->save($data);
        $session = session();
        $session->setFlashdata('msg', 'Data telah berjaya dikemaskini');
        return redirect()->to('/user-list');
    }

    function create() {
        $data = [
            'id'       => '',
            'name'     => '',
            'email'    => '',
            'password' => ''
        ];
        return view('user/form', [
            'data' => $data,
            'validator' => null
        ]);
    }

    function edit($id) {
        $id = my_decrypt($id);
        $user = $this->model->find($id);
        $user['password'] = ''; // x nak papar hashed pwd
        return view('user/form', [
            'data' => $user,
            'validator' => null
        ]);
    }

    function delete($id) {
        $id = my_decrypt($id);
        $this->model->delete($id);
        return redirect()->back(); // back() - return ke prev page
    }
}
