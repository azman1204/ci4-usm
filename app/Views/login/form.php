<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="/assets/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="container">

    <?php if (session()->has('msg')) : ?>
        <div class="alert alert-danger">
            <?= session()->getFlashdata('msg') ?>
        </div>
    <?php endif; ?>

    <?= form_open('/login') ?>
    <div class="row">
        <div class="col-md-6">
            <label>Emel</label>
            <?= form_input([
                'name' => 'email', 
                'class' => 'form-control'], 
                set_value('email', 'testing...')) 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>Katalaluan</label>
            <?= form_password(
                ['name' => 'password', 'class' => 'form-control'], 
                '') 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= form_submit(['value' => 'Login', 'class'=>'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
</body>
</html>