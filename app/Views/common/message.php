<?php if(isset($validator)): ?>
    <div class="alert alert-danger">
        <?= $validator->listErrors() ?>
    </div>
<?php endif; ?>