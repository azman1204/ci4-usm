<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<h1>Senarai Pengguna Sistem</h1>

<?php if(session()->has('msg')) : ?>
    <div class="alert alert-success">
        <?= session()->getFlashdata('msg') ?>
    </div>
<?php endif; ?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Bil.</th>
            <th>Nama</th>
            <th>Emel</th>
            <th>Tindakan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //$no = 1;
        foreach ($users as $user) : ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $user['name'] ?></td>
                <td><?= $user['email'] ?></td>
                <td>
                    <a href="/user-edit/<?= my_encrypt($user['id']) ?>">Edit</a>
                    <a href="/user-delete/<?= my_encrypt($user['id']) ?>">Delete</a>
                </td>
            </tr>
            <?php
        endforeach; ?>
    </tbody>
</table>
<?= $pager->links() ?>

<?= $this->endSection() ?>