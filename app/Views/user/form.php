<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<h1>Daftar Pengguna</h1>

<?= $this->setData(['validator'=>$validator])
         ->include('common/message') ?>

<?= form_open('/user-save') ?>
<?= form_hidden('id', $data['id']) ?>
<div class="row">
    <div class="col-md-6">
        <label>Nama</label>
        <input type="text" name="name" 
        class="form-control" value="<?= $data['name'] ?>">
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label>Emel</label>
        <input type="email" name="email" 
        class="form-control" value="<?= $data['email'] ?>">
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label>Katalaluan</label>
        <input type="password" name="password" 
        class="form-control" value="<?= $data['password'] ?>">
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-6">
        <input type="submit" value="Simpan" class="btn btn-primary">
    </div>
</div>
<?= form_close() ?>
<?= $this->endSection() ?>