<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<a href="/product-create" class="btn btn-primary btn-sm mb-2">Tambah Produk</a>
<table class="table table-bordered table-striped">
    <tr>
        <td>Bil.</td>
        <td>Nama</td>
        <td>Keterangan</td>
        <td>Harga</td>
        <td>Edit</td>
        <td>Hapus</td>
    </tr>
    <?php
    $bil = 1;
    foreach($rows as $prod): ?>
        <tr>
            <td><?= $bil++ ?></td>
            <td><?= $prod['name'] ?></td>
            <td><?= $prod['description'] ?></td>
            <td><?= $prod['price'] ?></td>
            <td>
                <a href="/product-edit/<?= $prod['id'] ?>" class="btn btn-success btn-sm">Edit</a>
            </td>
            <td>
                <a href="/product-del/<?= $prod['id'] ?>" class="btn btn-warning btn-sm">Hapus</a>
            </td>
        </tr>
        <?php
    endforeach; ?>
</table>

<?= $this->endSection() ?>