<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<?= $this->setData(['validator'=>$validator])
         ->include('common/message') ?>

<form action="/product-save" method="post">
    <input type="hidden" name="id" value="<?= $prod['id'] ?>">
    Nama:
    <input type="text" name="name" value="<?= $prod['name'] ?>">
    <br>

    Keterangan :
    <textarea name="description"><?= $prod['description'] ?></textarea>
    <br>

    Harga: 
    <input type="number" name="price" value="<?= $prod['price'] ?>">
    <br>

    <input type="submit" value="Simpan">
</form>

<?= $this->endSection() ?>