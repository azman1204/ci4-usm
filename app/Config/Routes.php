<?php
use CodeIgniter\Router\RouteCollection;

$routes->get('/lib-demo', 'HelperDemoController::calculateNewSalary');
$routes->get('/helper-demo', '\App\Controllers\HelperDemoController::index');
$routes->get('/3rd-party', 'HelperDemoController::print');
$routes->get('/pdf', 'HelperDemoController::genPdf');
$routes->get('/query-builder', 'HelperDemoController::queryBuilder');
$routes->get('/join-model', 'HelperDemoController::joinModel');

$routes->get('/ajax-call', function() {
    return view('ajax_call');
});

$routes->get('/ajax-call2/(:any)', 'HelperDemoController::ajaxCall/$1');

/**
 * @var RouteCollection $routes
 */
// http://ci4-tot2/
$routes->get('/', 'Home::index');

// http://ci4-tot2/home
$routes->get('/home', function() {
    return view('home'); // Views/home.php
});

// http://ci4-tot2/about-us.html
$routes->get('/about-us.html', function() {
    return view('about_us', [
        'siteName' => 'Acme Inc',
        'categories' => ['rockets', 'magnet', 'tools']
    ]);
});

// route ini hanya yg logged-in dgn role=admin atau client shj boleh masuk
$routes->get('/news/index', 'NewsController::index', 
['filter' => 'pakguard:admin,client']);

$routes->get('/news/create', 'NewsController::create');
// post() - biasa ada form
$routes->post('/news/save', 'NewsController::save');

$routes->get('/product-list', 'ProductController::index');
$routes->get('/product-create', 'ProductController::create');
$routes->post('/product-save', 'ProductController::save');
// http://ci4-tot2.test/product-edit/4
$routes->get('/product-edit/(:num)', 'ProductController::edit/$1');
$routes->get('/product-del/(:num)', 'ProductController::delete/$1');

// users
// http://ci4-tot2.test/user/user-list
// $routes->group('/user', ['filter' => 'pakguard'], function($routes) {
$routes->group('', ['filter' => ['pakguard:admin', 'audit']], function($routes) {
    $routes->get('/user-list', 'UserController::index');
    $routes->get('/user-create', 'UserController::create');
    $routes->post('/user-save', 'UserController::save');
    $routes->get('/user-edit/(:any)', 'UserController::edit/$1');
    $routes->get('/user-delete/(:any)', 'UserController::delete/$1');
});

// login
$routes->get('/login', 'LoginController::login');
$routes->post('/login', 'LoginController::auth');
$routes->get('/logout', 'LoginController::logout');