<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    // model ini mewakili table products
    protected $table = 'products';
    protected $allowedFields = [
        'name', 'description', 'price'
    ];
}
