<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array'; // object
    protected $useSoftDeletes   = false; // delete data dgn mark sbg delete
    protected $protectFields    = true;
    protected $allowedFields    = [
        'name', 'password', 'email'
    ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    protected array $casts = [];
    protected array $castHandlers = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at'; // utk soft delete

    // Validation
    protected $validationRules      = [
        'name'     => 'required',
        'email'    => "required|valid_email",
        'password' => 'required'
    ];
    protected $validationMessages   = [
        'name' => ['required' => 'nama wajib diisi']
    ];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks - function
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['janaKatalaluan'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ['janaKatalaluan'];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    function janaKatalaluan($data) {
        //var_dump($data);exit;
        if (isset($data['data']['id'])) {
            // update
            if ($data['data']['password'] == '') {
                // update pwd
                return $data;
            }
        }

        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
        return $data;
    }
}
