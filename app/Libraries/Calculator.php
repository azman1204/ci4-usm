<?php
namespace App\Libraries;

class Calculator {
    function darab($no1, $no2) {
        return $no1 * $no2;
    }

    function increaseSalary($salary, $pct) {
        return $salary + ($salary * $pct / 100);
    }
}