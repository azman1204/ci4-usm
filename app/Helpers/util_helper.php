<?php

function sum($no1, $no2) {
    return $no1 + $no2;
}

function minus($no1, $no2) {
    return $no1 - $no2;
}

function my_encrypt($val) {
    return base64_encode($val);
}

function my_decrypt($val) {
    return base64_decode($val);
}