<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterTableUser extends Migration
{
    // >php spark migrate
    public function up()
    {
        $this->forge->addColumn('users', ['role'  => ['type' => 'varchar', 'constraint' => '20']]);
    }

    // >php spark migrate:rollback
    public function down()
    {
        $this->forge->dropColumn('users', 'role');
    }
}
