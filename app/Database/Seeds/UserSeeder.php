<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $users = [
            ['name' => 'John Doe', 'email' => 'john@gmail.com', 'password' => '1234'],
            ['name' => 'Ali Bakar', 'email' => 'ali@gmail.com', 'password' => '1234'],
            ['name' => 'Abu Bakar', 'email' => 'abu@gmail.com', 'password' => '1234'],
            ['name' => 'Maimunah Bakar', 'email' => 'maimun@gmail.com', 'password' => '1234'],
        ];

        foreach($users as $user) {
            // query builder 
            $this->db->table('users')->insert($user);
        }
    }
}
